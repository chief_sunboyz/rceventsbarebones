package org.randomcraft.RCEventsBareBones;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.randomcraft.RCEventsBareBones.Commands.EventEnd;
import org.randomcraft.RCEventsBareBones.Commands.EventStart;
import org.randomcraft.RCEventsBareBones.Groups.InEvent;

public class RCEventsBareBones extends JavaPlugin implements Listener{

	
	
	static ArrayList<Player> ic = new ArrayList<Player>();
	
	public static ArrayList<Player> getIC(){
		return ic;
	}
	
	
	static ArrayList<Player> solved = new ArrayList<Player>();
	
	public static ArrayList<Player> getSolved(){
		return solved;
	}
	
	
	static ArrayList<Player> winner = new ArrayList<Player>();
	
	public static ArrayList<Player> getWinner(){
		return winner;
	}
	
	
	static ArrayList<Player> spec = new ArrayList<Player>();
	
	public static ArrayList<Player> getSpec(){
		return spec;
	}
	
	
	
	@SuppressWarnings("unused")
	public void onEnable() {
		
		PluginManager pm =Bukkit.getServer().getPluginManager();
		pm.registerEvents(this, this);
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		
		EventStart es = new EventStart();
		EventEnd en = new EventEnd();
		
		getCommand("eventstart").setExecutor(es);
		getCommand("eventend").setExecutor(en);
		
		getServer().getPluginManager().registerEvents(new InEvent(), this);
		
		
		
		getConfig().options().copyDefaults(true);
		saveConfig();
		reloadConfig();
	}
	
	public void onDisable() {
		
	}
}
